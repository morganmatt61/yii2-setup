<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use webvimark\modules\UserManagement\components\GhostNav;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse',
        ],
    ]);
 
    $navItems = [
        ['label' => 'Home', 'url' => ['/site/index'], 'visible' => true],
        ['label' => 'About', 'url' => ['/site/about'], 'visible' => true],
        ['label' => 'Contact', 'url' => ['/site/contact'], 'visible' => true],
        ['label' => 'Image Manager', 'url' => ['/imagemanager'], 'visible' => (Yii::$app->user->isGuest) ? false : true],
    ];

    if (Yii::$app->user->isGuest) {
        $navItems[] = ['label'=>'Login', 'url'=>['/user-management/auth/login']];
        $navItems[] = ['label'=>'Registration', 'url'=>['/user-management/auth/registration']];
    } else {
        $navItems[] = ['label' => 'User',
            'items'=>[
                ['label'=>'Logout', 'url'=>['/user-management/auth/logout']],
                ['label'=>'Change own password', 'url'=>['/user-management/auth/change-own-password']],
                ['label'=>'Password recovery', 'url'=>['/user-management/auth/password-recovery']],
                ['label'=>'E-mail confirmation', 'url'=>['/user-management/auth/confirm-email']],
            ]
        ];
        $navItems[] = [
            'label' => 'Roles/Permissions',
            'items'=> UserManagementModule::menuItems()
        ];
    } 

    echo GhostNav::widget([
        'encodeLabels'=>false,
        'activateParents'=>true,
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $navItems
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
